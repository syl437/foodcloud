// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.directives',
                'ngStorage', 'ngCordova', 'angularMoment'])

    .run(function ($localStorage, $timeout, $stateParams, $http, WebService, $ionicLoading, $ionicPlatform, $rootScope, AuthService, $state, $ionicPopup) {

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            var notificationOpenedCallback = function (jsonData) {

                // alert(JSON.stringify(jsonData));

               $state.go('app.calendar');

            };

            if (window.cordova){

                window.plugins.OneSignal.init("e7542820-9df0-4256-9b4b-e29e8ab3cbe7",
                    {googleProjectNumber: "898586517376"},
                    notificationOpenedCallback);

                window.plugins.OneSignal.getIds(function (ids) {
                    $rootScope.pushId = ids.userId;
                    if (AuthService.isAuthenticated()){
                        updatePushId();
                    }
                });
                // Show an alert box if a notification comes in when the user is in your app.
                window.plugins.OneSignal.enableInAppAlertNotification(true);

            }

        });


        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

            // first state at app launch

            if (toState.name !== "app.login" && !AuthService.isAuthenticated()) {

                event.preventDefault();

                $state.go('app.login');

            }

            // can't go to login if is already logged in

            if (toState.name === "app.login" && AuthService.isAuthenticated()) {

                event.preventDefault();
                $state.go('app.calendar');
                // $state.go('app.home');

            }

            // show cart icon on the top

            if (toState.name === 'app.product' || toState.name === 'app.store_category' || toState.name === 'app.store'){

                $rootScope.showCart = true;

            } else {

                $rootScope.showCart = false;

            }

            // get data before loading controller

            if (toState.name === "app.calendar") {
            // if (fromState.name === 'app.home' && toState.name === "app.calendar") {

                $rootScope.orders = [];
                getOrders();
                getRestaurants();

            }

            // if (fromState.name === 'app.restaurants' && toState.name === "app.categories") {

                    // getDishes();

            // }

            if (fromState.name === 'app.calendar' && toState.name === "app.employees" && $localStorage.employeeToOrder){
                delete $localStorage.employeeToOrder;
            }

        });

        // Data

        $rootScope.pushId = '';
        $rootScope.serverHost = 'http://tapper.co.il/foodcloud/laravel/public';
        // $rootScope.serverHost = 'http://tapper.co.il/foodcloud/php/';
        $rootScope.orders = [];
        $rootScope.eventOrders = [];
        $rootScope.event = {};
        $rootScope.editedEvent = {};
        $rootScope.showCart = false;
        $rootScope.cart = [];
        $rootScope.restaurants = [];
        $rootScope.firstDayOfWeek = moment().startOf('week');
        $rootScope.lastDayOfWeek = moment().endOf('week');
        $rootScope.alphabet = ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש'];
        $rootScope.dishes = [];
        $rootScope.mode = '';
        $rootScope.day_number = '';
        $rootScope.day = '';
        $rootScope.restaurant_id = '';
        $rootScope.category_id = '';
        $rootScope.order_id = '';
        $rootScope.eventDate = '';
        $rootScope.eventTime = '';
        // $rootScope.eventDate = moment().format('YYYY-MM-DD');
        // $rootScope.eventTime = moment().format('HH:mm');
        $rootScope.employeeToOrder = null;

        // Methods

        $rootScope.getOrders = getOrders;
        $rootScope.getDishes = getDishes;
        $rootScope.getRestaurants = getRestaurants;

        // Standard failure function for all controllers

        $rootScope.failure = function(){

            $ionicLoading.hide();
            $ionicPopup.alert({title: "אין התחברות!"});

        };
        
        function updatePushId() {
            $http.post(WebService.UPDATEPUSH, {"user_id": AuthService.getUser(), "push_id" : $rootScope.pushId}).then(function (data){
                console.log(JSON.stringify(data));
            })
        }

        // Functions

        function getRestaurants () {

            $http.post(WebService.RESTAURANTS, {"companyid": AuthService.getCompany()}).then(successRestaurants, $rootScope.failure)

        }

        function successRestaurants(data){

            $rootScope.restaurants = data;
            console.log('Restaurants', $rootScope.restaurants);

        }

        function getOrders () {

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                var user_id = AuthService.getUser();
                if ($rootScope.employeeToOrder){
                    user_id = $rootScope.employeeToOrder.index;
                }

                $http.post(WebService.GETORDERS, {

                    user_id : user_id,
                    from_date : $rootScope.firstDayOfWeek.format('YYYY-MM-DD'),
                    to_date : $rootScope.lastDayOfWeek.format('YYYY-MM-DD')

                }).then(successOrders, $rootScope.failure)

            })

        }

        function successOrders(data){

            $ionicLoading.hide();

            $rootScope.orders = data;

            for (var i = 0; i < $rootScope.orders.length; i++){

                $rootScope.orders[i].letter = $rootScope.alphabet[i];
                $rootScope.orders[i].date = moment($rootScope.orders[i].date_stateParams, 'YYYY-MM-DD');
                $rootScope.orders[i].number = i;

            }

            console.log('Orders', $rootScope.orders);

        }

        function getDishes () {

            $http.post(WebService.DISHES, {

                day_number : $rootScope.day_number,
                restaurant_id : $rootScope.restaurant_id

            }).then(successDishes, $rootScope.failure)

        }

        function successDishes(data){

            $rootScope.dishes = data;
            console.log('Dishes', $rootScope.dishes);

        }

    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

        $ionicConfigProvider.backButton.previousTitleText(false).text('');

        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('app.home', {
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home.html',
                        controller: 'HomeCtrl'
                    }
                }
            })

            .state('app.calendar', {
                url: '/calendar',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/calendar.html',
                        controller: 'CalendarCtrl'
                    }
                }
            })


            .state('app.restaurants', {
                url: '/restaurants/:mode/:day/:day_number/:order_id?',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/restaurants.html',
                        controller: 'RestaurantsCtrl'
                    }
                }
            })

            .state('app.categories', {
                url: '/categories/:mode/:day/:day_number/:restaurant_id/:order_id?',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/categories.html',
                        controller: 'CategoriesCtrl'
                    }
                }
            })

            .state('app.dishes', {
                url: '/dishes/:mode/:day/:day_number/:restaurant_id/:category_id/:order_id?',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/dishes.html',
                        controller: 'DishesCtrl'
                    }
                }
            })

            .state('app.store', {
                url: '/store',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/store.html',
                        controller: 'StoreCtrl'
                    }
                }
            })

            .state('app.store_category', {
                url: '/store_category/:store_id/:category_id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/store_category.html',
                        controller: 'StoreCategoryCtrl'
                    }
                }
            })

            .state('app.product', {
                url: '/product/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/product.html',
                        controller: 'ProductCtrl'
                    }
                }
            })

            .state('app.cart', {
                url: '/cart',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/cart.html',
                        controller: 'CartCtrl'
                    }
                }
            })

            .state('app.personal', {
                url: '/personal',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/personal.html',
                        controller: 'PersonalCtrl'
                    }
                }
            })

            .state('app.events', {
                url: '/events',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/events.html',
                        controller: 'EventsCtrl'
                    }
                }
            })

            .state('app.event', {
                url: '/event',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/event.html',
                        controller: 'EventCtrl'
                    }
                }
            })

            .state('app.event_edit', {
                url: '/event/:event_id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/event.html',
                        controller: 'EventCtrl'
                    }
                }
            })

            .state('app.employees', {
                url: '/employees',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/employees.html',
                        controller: 'EmployeesCtrl'
                    }
                }
            })

        ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/login');

        $httpProvider.interceptors.push('WebServiceHttpInterceptor');
    });
