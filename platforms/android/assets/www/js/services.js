angular.module('starter.services', [])

    .constant('LARAVEL_HOST', 'http://tapper.org.il/foodcloud/laravel/public/')
    // .constant('LARAVEL_HOST', 'http://tapper.co.il/foodcloud/laravel/public/')

    .service('AuthService', AuthService)

    .factory('WebService', WebService)

    .factory('WebServiceHttpInterceptor', WebServiceHttpInterceptor)

    .service('OrderService', OrderService);

function AuthService ($http, $localStorage, $q, WebService, $rootScope){

    // Public

    this.authenticate = authenticate;

    this.getUser = getUser;

    this.getCompany = getCompany;

    this.isAuthenticated = isAuthenticated;

    this.logout = logout;

    // Private

    function authenticate(idcard) {

        var deferred = $q.defer();

        $http.post(WebService.LOGIN, {idcard: idcard, push_id: $rootScope.pushId}).then(success, failure);

        function success(data) {

            // If credentials are invalid

            if (data.response.status != "1") {

                deferred.reject({error: 'invalidCredentials'});

                return;
            }

            // Else

            $localStorage.userid = data.response.userid;
            $localStorage.companyid = data.response.companyid;
            $localStorage.name = data.response.name;
            $localStorage.image = data.response.image;
            $localStorage.isManager = data.response.is_manager;

            deferred.resolve();

        }

        function failure(error) {

            deferred.reject();

        }

        return deferred.promise;

    }

    function logout() {

        delete $localStorage.userid;
        delete $localStorage.companyid;
        delete $localStorage.name;
        delete $localStorage.image;

    }

    function isAuthenticated() {
        return $localStorage.userid !== undefined;
    }

    function getUser() {
        return $localStorage.userid;
    }

    function getCompany() {
        return $localStorage.companyid;
    }

}



function WebService(LARAVEL_HOST){

    return {

        ADDEVENTSCHEDULE: LARAVEL_HOST + 'addEventSchedule',
        ADDSTOREORDER: LARAVEL_HOST + 'addStoreOrder',
        DELETEORDER: LARAVEL_HOST + 'deleteOrder',
        DISHES: LARAVEL_HOST + 'getDishes',
        DELETEEVENT: LARAVEL_HOST + 'deleteEvent',
        EMPLOYEES: LARAVEL_HOST + 'getEmployees',
        EVENTS: LARAVEL_HOST + 'getEvents',
        EVENTORDERS: LARAVEL_HOST + 'getEventOrders',
        GETEVENTSCHEDULE: LARAVEL_HOST + 'getEventSchedule',
        GETORDERS: LARAVEL_HOST + 'getOrders',
        LOGIN: LARAVEL_HOST + 'userLogin',
        MAKEORDER: LARAVEL_HOST + 'makeOrder',
        RESTAURANTS: LARAVEL_HOST + 'getUserResturants',
        STORE: LARAVEL_HOST + 'getStore',
        PRODUCT: LARAVEL_HOST + 'getProduct',
        PRODUCTS: LARAVEL_HOST + 'getProducts',
        UPDATECOMMENT: LARAVEL_HOST + 'updateComment',
        UPDATEEVENTSCHEDULE: LARAVEL_HOST + 'updateEventSchedule',
        UPDATEORDER: LARAVEL_HOST + 'updateOrder',
        UPDATEPUSH: LARAVEL_HOST + 'updatePushId',
        UPDATEUSER: LARAVEL_HOST + 'updateUserData',
        USER: LARAVEL_HOST + 'getUserData'

    }
}



function WebServiceHttpInterceptor($httpParamSerializerJQLike, LARAVEL_HOST){

    return {

        'request': function (config) {

            // For regular requests do nothing

            if (!config.url.startsWith(LARAVEL_HOST))
                return config;

            // For API requests transform it to applicable form

            config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
            config.paramSerializer = $httpParamSerializerJQLike;
            config.data = $httpParamSerializerJQLike(config.data);

            return config;
        },

        'response': function (data) {

            // For regular requests do nothing

            if (!data.config.url.startsWith(LARAVEL_HOST))
                return data;

            // For API requests transform it to applicable form

            return data.data

        }

    }

}

function OrderService ($rootScope){

    // Public

    this.orderForDay = orderForDay;

    // Private

    function orderForDay(day) {

        for (var i = 0; i < $rootScope.orders.length; i++){

                if ($rootScope.orders[i].order.order_date == day){

                    return $rootScope.orders[i].order.orders_index;

                }

        }

    }

}
