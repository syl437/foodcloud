angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($rootScope, $localStorage, $scope, AuthService, $state) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        $scope.isManager = $localStorage.isManager;

        $scope.goToCalendar = function (){
            if ($rootScope.employeeToOrder){
                $rootScope.employeeToOrder = null;
            }
            $state.go('app.calendar');
        };

        $scope.logout = function () {

            AuthService.logout();

            $state.go('app.login');

        };

    })

    .controller('CalendarCtrl', function ($ionicLoading, $ionicModal, $state, $ionicPopup, $localStorage, $http, $scope, $stateParams, $rootScope, WebService) {

        $scope.$on('$ionicView.enter', function(e) {

           if ($rootScope.orders.length == 0){
               $rootScope.getOrders();
           }

        });

        // Data

        $scope.updatedComment = {'content' : ''};
        $scope.infoOrder = {};

        // Methods

        $scope.addWeek = addWeek;
        $scope.subtractWeek = subtractWeek;
        $scope.showInfoPopup = showInfoPopup;
        $scope.hideInfoPopup = hideInfoPopup;
        $scope.updateComment = updateComment;
        $scope.checkAvailability = checkAvailability;
        $scope.checkAvailabilityForEdit = checkAvailabilityForEdit;
        $scope.checkAvailabilityForInfo = checkAvailabilityForInfo;
        $scope.deleteOrder = deleteOrder;

        // Functions

        function deleteOrder(x) {
            $http.post(WebService.DELETEORDER, {order_id : x}).then(successDeleteOrder, $rootScope.failure)
        }

        function successDeleteOrder(data) {

            if (data.status == '1'){

                $ionicPopup.alert({title: "נמחק בהצלחה"}).then(function(){

                    hideInfoPopup();
                    $rootScope.getOrders();

                });

            } else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});
        }

        function checkAvailability (day) {
            var nineHours = moment();
            nineHours.hour(9);
            nineHours.minute(0);
            nineHours.second(0);
            if (day.date.isBefore(moment(), 'days')){
                $ionicPopup.alert({title: "לא יכולות להתבצע הזמנות מהעבר"});
            } else if (day.date.isBefore(nineHours, 'seconds')) {
                $ionicPopup.alert({title: "לא התבצעו הזמנות אחרי השעה 9 בבוקר של אותו יום"});
            } else {
                $state.go('app.restaurants', {mode: 'create', day: day.date_stateParams, day_number: day.number});
            }
        }

        function checkAvailabilityForEdit (day) {
            var nineHours = moment();
            nineHours.hour(9);
            nineHours.minute(0);
            nineHours.second(0);
            if (day.date.isBefore(moment(), 'days')){
                $ionicPopup.alert({title: "אי אפשר לערוך הזמנות מהעבר"});
            } else if (day.date.isBefore(nineHours, 'seconds')) {
                $ionicPopup.alert({title: "אין אפשרות לערוך או לבטל הזמנות שבוצעו לאותו יום אחרי השעה 9"});
            } else {
                $state.go('app.restaurants', {mode: 'edit', day: day.date_stateParams, day_number: day.number, order_id: day.order.orders_index});
            }
        }

        function checkAvailabilityForInfo () {
            if (typeof $scope.infoOrder.date != 'undefined'){
                var nineHours = moment();
                nineHours.hour(9);
                nineHours.minute(0);
                nineHours.second(0);
                if ($scope.infoOrder.date.isBefore(moment(), 'days')){
                    return false;
                } else if ($scope.infoOrder.date.isBefore(nineHours, 'seconds')) {
                    return false;
                }
                return true;
            }

        }

        function addWeek(){

            $rootScope.firstDayOfWeek = moment($rootScope.firstDayOfWeek).add(7, 'days');
            $rootScope.lastDayOfWeek = moment($rootScope.lastDayOfWeek).add(7, 'days');

            $rootScope.getOrders();

        }

        function subtractWeek (){

            $rootScope.firstDayOfWeek = moment($rootScope.firstDayOfWeek).subtract(7, 'days');
            $rootScope.lastDayOfWeek = moment($rootScope.lastDayOfWeek).subtract(7, 'days');

            $rootScope.getOrders();

        }

        function showInfoPopup (x){

            $scope.infoOrder = $rootScope.orders[x];
            $scope.updatedComment.content = $scope.infoOrder.order.comments;
            console.log($scope.infoOrder);

            $scope.infoPopup = $ionicPopup.show({
                templateUrl: 'templates/popups/info.html',
                scope: $scope,
                cssClass: 'infoPopup'
            });

        }

        function hideInfoPopup () {

            $scope.infoPopup.close();
            $scope.infoOrder = {};

        }

        function updateComment (x) {

            $http.post(WebService.UPDATECOMMENT, {

                order_id : x,
                comment : $scope.updatedComment.content

            }).then(successUpdateComment, $rootScope.failure)

        }

        function successUpdateComment(data){

            if (data.status == '1'){

                $ionicPopup.alert({title: "התעדכן בהצלחה"}).then(function(){

                    hideInfoPopup();
                    $rootScope.getOrders();

                });

            } else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});

        }

    })

    .controller('CartCtrl', function ($ionicPopup, $httpParamSerializerJQLike, $localStorage, $state, $rootScope, $http, WebService, $scope) {

        $scope.$on('$ionicView.enter', function(e) {

            console.log($rootScope.cart);

        });

        // Data



        // Methods

        $scope.deleteItem = deleteItem;
        $scope.addStoreOrder = addStoreOrder;

        // Functions

        function deleteItem(x){

            for (var i = 0; i < $rootScope.cart.length; i++){

                if (x == $rootScope.cart[i].index){

                    $rootScope.cart.splice(i, 1);

                }

            }

        }

        function addStoreOrder(){

            $http.post(WebService.ADDSTOREORDER, {

                user_id : $localStorage.userid,
                company_id : $localStorage.companyid,
                cart: $rootScope.cart

            }).then(successAddStoreOrder, $rootScope.failure)

        }

        function successAddStoreOrder(data){

            $ionicPopup.alert({title: "הקניה בוצעה בהצלחה!"});

            $rootScope.cart = [];

        }

    })

    .controller('CategoriesCtrl', function ($scope, $stateParams, $rootScope) {

        $scope.$on('$ionicView.enter', function(e) {

            $rootScope.mode = $stateParams['mode'];
            $rootScope.restaurant_id = $stateParams['restaurant_id'];
            $rootScope.day = $stateParams['day'];
            $rootScope.day_number = $stateParams['day_number'];
            $rootScope.order_id = $stateParams['order_id'] ? $stateParams['order_id'] : "0";

            $rootScope.getDishes();

        });

    })

    .controller('DishesCtrl', function (AuthService, OrderService, $state, $http, WebService, $scope, $stateParams, $rootScope, $localStorage, $ionicPopup) {

        $scope.$on('$ionicView.enter', function(e) {

            $scope.currentDishes = $rootScope.dishes[$stateParams['category_id']];
            console.log($scope.currentDishes);

            checkDishes();

            $rootScope.mode = $stateParams['mode'];
            $rootScope.day_number = $stateParams['day_number'];
            $rootScope.day = $stateParams['day'];
            $rootScope.restaurant_id = $stateParams['restaurant_id'];
            $rootScope.category_id = $stateParams['category_id'];
            $rootScope.order_id = $stateParams['order_id'] ? $stateParams['order_id'] : "0";

        });

        $scope.$on('$ionicView.leave', function(e) {

            $scope.currentDishes = [];
            $scope.selection = 'main';
            $scope.selectedDishes = {main : '', mod1 : '', mod2 : [], drink : '', fastfood: '', quantity : 1, comments : ''};
            $scope.editedOrder = {};

        });

        // Data

        $scope.selection = 'main';
        $scope.selectedDishes = {main : '', mod1 : '', mod2 : [], drink : '', fastfood: '', quantity : 1, comments : ''};
        $scope.editedOrder = {};
        $scope.orderExists = false;

        // Methods

        $scope.selectTab = selectTab;
        $scope.makeOrder = makeOrder;
        $scope.addMain = addMain;
        $scope.addMod1 = addMod1;
        $scope.addMod2 = addMod2;
        $scope.deleteMod2 = deleteMod2;
        $scope.addDrink = addDrink;
        $scope.addFastfood = addFastfood;
        $scope.isCheckboxSelected = isCheckboxSelected;
        $scope.addComment = addComment;

        // Functions


        function checkDishes(data){

            if ($scope.mode === 'edit'){

                for (var i = 0; i < $rootScope.orders.length; i++){

                    if ($rootScope.orders[i].type == 'ordered' && $rootScope.orders[i].order.orders_index == $scope.order_id)
                        $scope.editedOrder = $rootScope.orders[i].order;

                }

            }

            if ($scope.mode === 'editevent'){

                for (var j = 0; j < $rootScope.eventOrders.length; j++ ){

                    if ($rootScope.eventOrders[j].orders_index == $scope.order_id)

                        $scope.editedOrder = $rootScope.eventOrders[j];

                }

            }

            if (!angular.equals($scope.editedOrder, {})
                && $scope.category_id == $scope.editedOrder.food_type
                && $scope.restaurant_id == $scope.editedOrder.restaurant_id
                && $scope.day == $scope.editedOrder.order_date){

                $scope.orderExists = true;
            }

            if ($scope.orderExists){

                $scope.selectedDishes = {
                    main : $scope.editedOrder.main.index,
                    mod1 : $scope.editedOrder.mod1.index,
                    mod2 : [$scope.editedOrder.mod2_first.index],
                    drink : $scope.editedOrder.drink.index,
                    fastfood : $scope.editedOrder.fastfood.index,
                    comments : $scope.editedOrder.comments,
                    quantity : Number($scope.editedOrder.quantity)
                };

                if ($scope.editedOrder.mod2_second != '0'){
                    $scope.selectedDishes.mod2.push($scope.editedOrder.mod2_second.index);
                }

                console.log($scope.editedOrder);
                console.log($scope.selectedDishes);

            }

        }

        function selectTab (x) {$scope.selection = x;}

        function addMain (x) {

            if ($scope.mode === 'create' || ($scope.mode === 'edit' && $scope.orderExists) || $scope.mode === 'event' || ($scope.mode === 'editevent' && $scope.orderExists)) {

                $scope.selectedDishes.main = x;

            } else
                showDecisionPopup();

        }

        function addMod1 (x) {

            if ($scope.mode === 'create' || ($scope.mode === 'edit' && $scope.orderExists) || $scope.mode === 'event' || ($scope.mode === 'editevent' && $scope.orderExists)) {

                $scope.selectedDishes.mod1 = x;

            } else {

                showDecisionPopup();

            }

        }

        function addMod2 (x) {

            if ($scope.selectedDishes.mod2.length >= 2) {

                $ionicPopup.alert({
                    title: "אתה לא יכול לבחור יותר מ2 מנות מאותה קטגורייה",
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive'
                    }]
                });

                return;

            }

            if ($scope.mode === 'create' || ($scope.mode === 'edit' && $scope.orderExists) || $scope.mode === 'event' || ($scope.mode === 'editevent' && $scope.orderExists)) {

                $scope.selectedDishes.mod2.push(x);

            } else {

                showDecisionPopup();

            }

        }

        function deleteMod2 (x) {

            for (var i = 0; i < $scope.selectedDishes.mod2.length; i++) {

                if ($scope.selectedDishes.mod2[i] == x) {

                    $scope.selectedDishes.mod2.splice(i, 1);

                }

            }

        }

        function isCheckboxSelected (x) {

            for (var i = 0; i < $scope.selectedDishes.mod2.length; i++) {

                if ($scope.selectedDishes.mod2[i] == x) {

                    return true;

                }

            }

        }

        function addDrink (x) {

            if ($scope.mode === 'create' || ($scope.mode === 'edit' && $scope.orderExists) || $scope.mode === 'event' || ($scope.mode === 'editevent' && $scope.orderExists)) {

                $scope.selectedDishes.drink = x;

            } else {

                showDecisionPopup();

            }

        }

        function addFastfood (x) {

            if ($scope.mode === 'create' || ($scope.mode === 'edit' && $scope.orderExists) || $scope.mode === 'event' || ($scope.mode === 'editevent' && $scope.orderExists)) {

                $scope.selectedDishes.fastfood = x;

            } else
                showDecisionPopup();

        }

        function addComment () {

            $ionicPopup.show({
                template: '<textarea auto-grow class="rtl" ng-model="selectedDishes.comments"></textarea>',
                title: 'הערות להזמנה',
                scope: $scope,
                buttons: [
                    { text: 'סגור' },
                    {text: '<b>שמור</b>', type: 'button-positive'}
                ]
            });

        }

        function showDecisionPopup (){

            $ionicPopup.confirm({
                title: 'You already have order in other category or restaurant',
                template: 'Do you want to cancel your previous order and make a new one?',
                scope: $scope,
                buttons: [
                    {text: 'No'},
                    {
                        text: '<b>Yes</b>',
                        type: 'button-positive',
                        onTap: function (res) {deleteOrder();}
                    }
                ]
            });

        }

        function makeOrder () {

            console.log($scope.dishes);
            console.log($scope.selectedDishes);
            console.log($scope.category_id);

            if ($scope.category_id == '3' && $scope.selectedDishes.fastfood == ''){

                $ionicPopup.alert({title: "שכחת לבחור " + $scope.currentDishes.main.name + " , אנא בחר מנה",});
                return;

            }

            if ($scope.category_id != '3' && $scope.selectedDishes.main == ''){

                $ionicPopup.alert({title: "שכחת לבחור " + $scope.currentDishes.main.name + " , אנא בחר מנה"});
                return;

            }

            if ($scope.category_id != '3' && $scope.selectedDishes.mod1 == ''){

                $ionicPopup.alert({title: "שכחת לבחור " + $scope.currentDishes.mod1.name + " אנא בחר מנה"});
                return;

            }

            if ($scope.category_id != '3' && $scope.selectedDishes.mod2.length == 0){

                $ionicPopup.alert({title: "בבקשה לבחור לפחות אחד " + $scope.currentDishes.mod2.name});

                return;

            }

            if ($scope.category_id != '3' && $scope.selectedDishes.drink == ''){

                $ionicPopup.alert({title: "שכחת לבחור " + $scope.currentDishes.drink.name + " אנא בחר מנה",});
                return;

            }

            if ($scope.mode === 'create'  || $scope.mode === 'event'){

                var user_id = AuthService.getUser();
                if ($rootScope.employeeToOrder){
                    user_id = $rootScope.employeeToOrder.index;
                }

                $http.post(WebService.MAKEORDER, {

                    user_id : user_id,
                    company_id : $localStorage.companyid,
                    restaurant_id : $scope.restaurant_id,
                    category_id : $scope.category_id,
                    main : $scope.selectedDishes.main,
                    mod1 : $scope.selectedDishes.mod1,
                    mod2_first : typeof $scope.selectedDishes.mod2[0] !== 'undefined' ? $scope.selectedDishes.mod2[0] : '0',
                    mod2_second : typeof $scope.selectedDishes.mod2[1] !== 'undefined' ? $scope.selectedDishes.mod2[1] : '0',
                    drink : $scope.selectedDishes.drink,
                    fastfood : $scope.selectedDishes.fastfood,
                    comments : $scope.selectedDishes.comments,
                    quantity : $scope.selectedDishes.quantity,
                    event: $scope.mode == 'event' ? '1' : '0',
                    event_id: $rootScope.event.id ? $rootScope.event.id : '0',
                    order_date : $scope.day

                }).then(successMakeOrder, $rootScope.failure)

            } else {

                $http.post(WebService.UPDATEORDER, {

                    order_id : $scope.editedOrder.orders_index,
                    restaurant_id : $scope.restaurant_id,
                    category_id : $scope.category_id,
                    main : $scope.selectedDishes.main,
                    mod1 : $scope.selectedDishes.mod1,
                    mod2_first : $scope.selectedDishes.mod2[0],
                    mod2_second : typeof $scope.selectedDishes.mod2[1] !== 'undefined' ? $scope.selectedDishes.mod2[1] : '0',
                    drink : $scope.selectedDishes.drink,
                    fastfood : $scope.selectedDishes.fastfood,
                    quantity : $scope.selectedDishes.quantity,
                    comments : $scope.selectedDishes.comments

                }).then(successMakeOrder, $rootScope.failure)

            }

        }

        function successMakeOrder (data){

            if (data.status == '1'){

                $ionicPopup.alert({title: "פעולה בוצע בהצלחה"}).then(function(){

                    if ($scope.mode === 'create' || $scope.mode === 'edit'){

                        if ($rootScope.employeeToOrder){
                            $rootScope.employeeToOrder = null;
                            $state.go('app.employees');
                        } else {
                            $rootScope.getOrders();
                            $state.go('app.calendar');
                        }

                    } else
                        $state.go('app.event');

                });

            } else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});

        }

        function deleteOrder(){

            $http.post(WebService.DELETEORDER, {order_id : $scope.order_id}).then(successDeleteOrder, $rootScope.failure)

        }

        function successDeleteOrder (data){

            if (data.status == '1'){

                $ionicPopup.alert({title: "נמחק בהצלחה"});

                $scope.mode = 'create';
                $scope.editedOrder = {};
                $scope.orderExists = false;

            } else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});


        }

    })

    .controller('EmployeesCtrl', function ($localStorage, $rootScope, $http, WebService, $ionicPopup, $state, $scope) {

        $scope.$on('$ionicView.enter', function(e) {
            getEmployees();
        });

        $scope.employees = [];
        $scope.goToCalendar = goToCalendar;

        function goToCalendar (employee) {
            $rootScope.employeeToOrder = employee;
            $state.go('app.calendar');
        }

        function getEmployees() {
            $http.post(WebService.EMPLOYEES, {userid : $localStorage.userid}).then(successGetEmployees, $rootScope.failure)
        }

        function successGetEmployees(data) {
            console.log(data);
            $scope.employees = data;
        }

    })


    .controller('EventCtrl', function ($stateParams, $cordovaDatePicker, $localStorage, $rootScope, $http, WebService, $ionicPopup, $state, $scope) {

        $scope.$on('$ionicView.enter', function(e) {

            if ($state.current.name === 'app.event_edit')
                getEventOrders($stateParams.event_id);
            else
                getEventOrders($rootScope.event.id);

            if ($rootScope.restaurants.length == 0)
                $rootScope.getRestaurants();

        });

        // Data
        $scope.angular = angular;

        // Methods

        $scope.addEventSchedule = addEventSchedule;
        $scope.deleteEventOrder = deleteEventOrder;
        $scope.finishOrder = finishOrder;
        $scope.updateDateTime = updateDateTime;

        // Functions

        // function getEventSchedule(){
        //
        //     $http.post(WebService.GETEVENTSCHEDULE, {user_id : $localStorage.userid}).then(successEventSchedule, $rootScope.failure)
        //
        // }
        //
        // function successEventSchedule(data){
        //
        //     $rootScope.event = data;
        //     $rootScope.event.day_number = moment($rootScope.event.event_date, 'YYYY-MM-DD').day();
        //     $scope.eventDateTime = {date : moment(data.event_date, 'YYYY-MM-DD').toDate(), time : moment(data.event_time, 'HH:mm:ss').toDate()};
        //
        //     getEventOrders();
        //
        // }

        function updateDateTime (mode){

            var options = {
                date: new Date(),
                mode: mode === 'date' ? 'date' : 'time'
            };

            $cordovaDatePicker.show(options).then(function(data){

                if (mode === 'date')
                    $rootScope.eventDate = moment(data).format('YYYY-MM-DD');
                else
                    $rootScope.eventTime = moment(data).format('HH:mm');

            }, function (data) {console.log(data);});

        }

        function addEventSchedule(x){

            if ($rootScope.eventDate === '' || $rootScope.eventTime === '') {

                $ionicPopup.alert({title: "Please select date and time!"});
                return;

            }

            if (moment($rootScope.eventDate).isBefore(moment(), 'day')) {

                $ionicPopup.alert({title: "Selected date is in past!"});
                return;

            }

            if (x === 0) {

                $http.post(WebService.ADDEVENTSCHEDULE, {

                    user_id : $localStorage.userid,
                    date : $rootScope.eventDate,
                    time: $rootScope.eventTime

                }).then(successAddEventSchedule, $rootScope.failure)

            } else {

                $http.post(WebService.UPDATEEVENTSCHEDULE, {

                    event_id : $rootScope.event.id,
                    date : $rootScope.eventDate,
                    time: $rootScope.eventTime

                }).then(successAddEventSchedule, $rootScope.failure);

            }

        }

        function successAddEventSchedule(data){

            if (data.status == '1'){

                $rootScope.event = data.event;

                if ($rootScope.event.index)
                    $rootScope.event.id = $rootScope.event.index;

                $state.go('app.restaurants', {mode : 'event', day: $rootScope.eventDate, day_number: moment($rootScope.eventDate, 'YYYY-MM-DD').day()});

            }
            else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});


        }

        function getEventOrders(id){

            $http.post(WebService.EVENTORDERS, {event_id : id}).then(successEventOrders, $rootScope.failure)

        }

        function successEventOrders(data){

            $rootScope.eventOrders = data.orders;
            $rootScope.editedEvent = data.event;
            console.log('eventOrders', data);

            if ($state.current.name === 'app.event_edit'){

                $rootScope.eventDate = $rootScope.editedEvent.event_date;
                $rootScope.eventTime = $rootScope.editedEvent.event_time;

            }

        }

        function deleteEventOrder(x){

            $http.post(WebService.DELETEORDER, {order_id : x}).then(successDelete, $rootScope.failure)

        }

        function successDelete(data){

            if (data.status == '1'){

                $ionicPopup.alert({title: "נמחק בהצלחה"});
                getEventOrders();

            } else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});

        }
        
        function finishOrder() {

            $http.post(WebService.UPDATEEVENTSCHEDULE, {

                event_id : $rootScope.event.id,
                date : $rootScope.eventDate,
                time: $rootScope.eventTime,
                finished: '1'

            }).then(successFinishOrder, $rootScope.failure);

        }

        function successFinishOrder(data) {

            if (data.status == '1'){
                $state.go('app.events');
            }
            else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});
        }

    })

    .controller('EventsCtrl', function ($ionicPopup, $localStorage, $rootScope, WebService, $http, $scope) {

        $scope.$on('$ionicView.enter', function(e) {

            getEvents();

        });

        $scope.events = [];
        $scope.selection = '1';

        $scope.selectTab = selectTab;
        $scope.deleteEvent = deleteEvent;

        function selectTab (x) {$scope.selection = x;}

        function getEvents(){$http.post(WebService.EVENTS, {user_id : $localStorage.userid}).then(successEvents, $rootScope.failure)}

        function successEvents(data){

            angular.forEach(data, function (item) {

                item.event_date = moment(item.event_date, 'YYYY-MM-DD');
                item.event_time = moment(item.event_time, 'HH:mm:ss');
                item.future = moment(item.event_date).isSameOrAfter(moment(), 'day');

            });

            $scope.events = data;
            console.log('Events', $scope.events);

        }

        function deleteEvent(id) {

            $http.post(WebService.DELETEEVENT, {event_id : id}).then(successDeleteEvent, $rootScope.failure) 

        }
        
        function successDeleteEvent(data) {

            if (data.status === '1')
                getEvents();
            else
                $ionicPopup.alert({title: "שגיאה - אנא נסה שנית"});

        }

    })

    .controller('HomeCtrl', function ($scope) {

    })

    .controller('LoginCtrl', function (AuthService, $http, $state, $scope, $rootScope, $localStorage, $ionicLoading, $ionicPopup) {

        // Data

        $scope.login = {"idcard": ""};

        // Methods

        $scope.makeLogin = makeLogin;

        // Functions

         function makeLogin () {

             // if phone is empty

             if ($scope.login.idcard == "") {

                 $ionicPopup.alert({title: "נא למלא את כל השדות"});
                 return;

             }

             // if everything is OK

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            }).then(function(){

                    AuthService.authenticate($scope.login.idcard).then(success, failure);

                });

         }

         function success(){

             $state.go('app.calendar');
             // $state.go('app.home');
             $scope.login = {"idcard": ""};
             $ionicLoading.hide();

         }

        function failure(errors){

            if (!errors) {

                $ionicLoading.hide();

                $ionicPopup.alert({title: "אין התחברות!"});
                return;

            }

            if (errors.error == 'invalidCredentials') {

                $ionicLoading.hide();

                $ionicPopup.alert({title: "נתונים לא נכונים!"});

            }

        }

    })

    .controller('PersonalCtrl', function ($ionicPopup, $http, WebService, $scope, $rootScope, $localStorage) {

        $scope.$on('$ionicView.enter', function(e) {

            getUserData();

        });

        // Data

        $scope.userdata = {};
        $scope.newdata = {name : "", phone : "", email : ""};

        // Methods

        $scope.updateUserData = updateUserData;

        // Functions

        function getUserData () {

            $http.post(WebService.USER, {"userid" : $localStorage.userid}).then(success, $rootScope.failure)

        }

        function success(data){

            $scope.userdata = data[0];
            $scope.newdata = {name : $scope.userdata.name, phone : $scope.userdata.phone, email : $scope.userdata.email};

            console.log('Userdata', $scope.userdata);

        }

        function updateUserData () {

            $http.post(WebService.UPDATEUSER, {
                userid : $localStorage.userid,
                name : $scope.newdata.name,
                phone : $scope.newdata.phone,
                email : $scope.newdata.email
            }).then(successUpdate, $rootScope.failure)

        }

        function successUpdate(data){

            if (data == 1)
                $ionicPopup.alert({title: "התעדכן בהצלחה"});
            else
                $ionicPopup.alert({title: "אין עדכונים אנא נסה שנית"});

        }

    })

    .controller('ProductCtrl', function ($rootScope, $http, WebService, $scope, $stateParams) {

        $scope.$on('$ionicView.enter', function(e) {

            getProduct();

        });

        // Data

        $scope.id = $stateParams.id;
        $scope.product = {};
        $scope.quantity = 1;
        $scope.comments = {content: ''};

        // Methods

        $scope.incrementQuantity = incrementQuantity;
        $scope.decrementQuantity = decrementQuantity;
        $scope.addToCart = addToCart;
        $scope.isInCart = isInCart;

        // Functions

        function getProduct () {

            $http.post(WebService.PRODUCT, {"productid" : $scope.id}).then(success, $rootScope.failure)

        }

        function success(data){

            $scope.product = data;
            console.log('Product', $scope.product);

        }

        function incrementQuantity(){

            $scope.quantity += 1;

        }

        function decrementQuantity(){

            if ($scope.quantity > 1)
                $scope.quantity -= 1;

        }

        function addToCart(x){

            x.quantity = $scope.quantity;
            x.comments = $scope.comments.content;

            $rootScope.cart.push(x);

        }

        function isInCart(x){

            for (var i = 0; i < $rootScope.cart.length; i++){

                if ($rootScope.cart[i].index == x)
                    return true;

            }

        }

    })

    .controller('RestaurantsCtrl', function ($ionicPopup, $state, $stateParams, WebService, AuthService, $http, $scope, $rootScope) {

        $scope.$on('$ionicView.enter', function(e) {

            $rootScope.mode = $stateParams['mode'];
            $rootScope.day = $stateParams['day'];
            $rootScope.day_number = $stateParams['day_number'];
            $rootScope.order_id = $stateParams['order_id'] ? $stateParams['order_id'] : "0";
            $scope.image = $scope.images[Math.floor(Math.random() * $scope.images.length)];
            console.log($scope.image);

        });

        $scope.checkAvailability = checkAvailability;
        $scope.images = [
            'http://tapper.co.il/foodcloud/slider_images/1.jpg',
            'http://tapper.co.il/foodcloud/slider_images/2.jpg',
            'http://tapper.co.il/foodcloud/slider_images/3.jpg',
            'http://tapper.co.il/foodcloud/slider_images/4.jpg'
        ];

        function checkAvailability(restaurant){
            var day = moment($stateParams['day'], 'YYYY-MM-DD');

            var nineHours = moment();
            nineHours.hour(9);
            nineHours.minute(0);
            nineHours.second(0);

            if (day.isSame(moment(), 'days')){
                if (restaurant.same_day == '1' && moment().isBefore(nineHours, 'seconds')){
                    $state.go('app.categories', {mode: $stateParams['mode'], day: $stateParams['day'], day_number: $stateParams['day_number'], restaurant_id: restaurant.index, order_id: $rootScope.order_id});
                } else {
                    $ionicPopup.alert({title: "המטבח הזה לא מקבל יותר הזמנות להיום"});
                }
            } else if (day.isAfter(moment(), 'days')){
                $state.go('app.categories', {mode: $stateParams['mode'], day: $stateParams['day'], day_number: $stateParams['day_number'], restaurant_id: restaurant.index, order_id: $rootScope.order_id});
            }
        }


    })

    .controller('StoreCategoryCtrl', function ($rootScope, $stateParams, $ionicPopup, WebService, AuthService, $http, $scope) {

        $scope.$on('$ionicView.enter', function(e) {

            getProducts();

        });

        // Data

        $scope.store_id = $stateParams.store_id;
        $scope.category_id = $stateParams.category_id;
        $scope.products = [];

        // Methods


        // Functions

        function getProducts () {

            $http.post(WebService.PRODUCTS, {"storeid" : $scope.store_id, "categoryid": $scope.category_id}).then(success, $rootScope.failure)

        }

        function success(data){

            $scope.products = data;
            console.log('Products', $scope.products);

        }

    })

    .controller('StoreCtrl', function ($rootScope, $ionicPopup, WebService, AuthService, $http, $scope) {

        $scope.$on('$ionicView.enter', function(e) {

            getStore();

        });

        // Data

        $scope.store = [];

        // Methods


        // Functions

        function getStore () {

            $http.post(WebService.STORE, {"companyid": AuthService.getCompany()}).then(success, $rootScope.failure)

        }

        function success(data){

            $scope.store = data;
            console.log('Store', $scope.store);

        }

    })
;
